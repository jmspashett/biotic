A simple Basic to C transpiler
==============================

This is an "example" toy transpiler, and not meant for "production". The idea
being to show the workings. Some experimental non-basic syntax may be added
later. It was created to execute basic programs from the book "Computer
Battlegames". Usborune publishing Ltd. 1982. I had a similar book as a child,
but cannot remember which one it was. Furthermore I had access to a commodore
64, thus, the dialect of basic is Commodore 64/VIC/PET.

![Main book cover of Computer Battlegames](main-cover.png)
![First program inside book](robot-missile.png)


Stages
------

Roughly speaking, this is what the transpiler does:
1. Execute the main function:
2. Call parse_statement
    *. if no tokens remaining:
       *. fetch a line into a string
       *. Generate a list of tokens
       *. parse line number
    *. Parse the tokens using a recursive LR parser
        *. emit C source code as the parsing happens, mostly calling codeGen_*
        functions. 
		*. for the control keyword experiment, recursively call 2, as decribed
        in [crenshaw-13]
    *. goto 2.

Notes
-----

* The role of reserved words: The lexical analyzer does not have any reserved
  words defined in in it's lexical analyzer. However, basic does define
  reserved words. What this means is that you cannot say IF = 5, because the
  parser recognizes the IF keyword as a statement. However, in expression
  context. e.g. PRINT IF = 5 the IF will be evaluated as an expression. This is
  the role of reserved words - to reserve them in all contexts. One aspect of
  basic which makes parsing without reserved words more fiddly is the FN
  keyword which can appear without surrounding spaces. e.g.  (a) DEF FNA(x) = 2
  * x verses: (b) DEF FN A(x) = 2 * x Implementing reserved words would help
  here in that FN would be defined as a reserved identifier, yielding FN as a
  separate tokens, vs what happens now, which is the token in case (a) is FNA
  A quick and dirty reserved word scan for "FN" has been put in the function 
  lex_identifier.
* conway.bas was modified to add a DIM statement. It appears to contain a bug
  since it accesses array elements at index 11, while the array itself should
  only be 10 elements (1-10 index)
  https://github.com/norvig/pytudes/blob/master/ipynb/BASIC.ipynb

Bugs
----

1. The compiler is a bit "loose" and may accept things that  it shouldn't
   particular look out for:
    a. basic syntax that doesn't generate correct C output
	b. At least at the moment. a = 5 b = 6 "works", in C64 basic this is not
	   allowed.
    c. Otherwise its perfect.
2. String concatenaton is NOT SAFE and can overflow buffers
3. The print function handing of ; and , may not be correct, or what is
   expected. 


To do
-----

* Parse_arg vs parse_sub-expression are very similar
* Update EBNF

EBNF+
----

```
<expression> ::= <math-expression> { <relop> <math-expression> ] }
<math-expression> :: = <term> { <add-or-sub> <term> }
<relop> ::= "<" | ">" | "="
<add-or-sub> :: = "+" | "-"
<term> ::= <factor> { <mul-or-div> <factor> } 
<mul-or-div> ::= "*" | "/"
<factor> ::= ["+" | "-"] ( <built-in-func> | <variable> | <literal> | <sub-expression> )
<built-in-func> ::= "chr$" | "int" | "rnd" "(" <expression> ")"  
<variable> ::= <special> | <str-var> | <real-var> | <array>  
<special> :: = "rnd"
<str-var> :: = /[a-zA-Z]+/ '$'
<real-var> :: = /[a-zA-Z]+/
<literal> := <numeric-literal> | <string-literal>
<numeric-literal> : = /[0-9]+/
<string-literal> := '"' /[^"]*/ '"' 
```

Observations
------------

* It is desirable to fix a specific thing in each git commit, however, this
  isn't always a quick thing to organise, especially if a refactoring also
  makes a fix. This comes up a lot, and it can take time to create coherent
  commits especially when code it changing rapidly, even with git's rebase and
  per hunk commits. This tends to happen early on when developing new code.
* When adding array variables, tests have now become most useful so as not to
  break existing functionality. There comes a point where automated test yields
  an X times benefit, whereas in the early stages of development they can
  hinder ease of "code re-architecture". For the same reason, the tests are all
  integration tests, testing compiler functionality, rather than internal
  units. This all makes the code easy to refactor without having to change unit
  tests, but with the usefulness of detecting breakages - integration type
  tests are underrated.

Experimental syntax
-------------------

The idea is to eventually add some experimental syntax that has nothing to do
with the basic language. As a pre-cursor to this, the following was implemented
to enable further sorts of future looping and local control forms.

Structured-control block
------------------------
```
<control-block> ::= "CONTROL" <statements> "END CONTROL"
<break-statement> ::= "LEAVE"
<again-statment> ::= "AGAIN"
```

A universal control block, which will provide all loop forms, and local
structured exit using LEAVE and EXIT. This also tests out the crenshaw method
of storing entry/exit labels by means of a recursive call, with a parameter
recording the target labels. It's not a method I actually like, despite
crenshaw's enthusiasm for it, because it essentially makes use of the
implementation languages' stack for code generation, where previously (in a LR
recursive parser) it is solely used for parsing. It would also *appear* to make
a break <to-label> more difficult than using an explicit stack.

With the exception of the structured exit, it's plain to see why explicit for
and while loops exist. It is easy to make a mistake in creating a manual for,
while or do loop using an abstract CONTROL type structure.

The structured exit below, however is more interesting, it is what programmers
in C sometimes do with a do { ... } while(false); loop to provide easy clean up
or early exit. This facility is missing in C, and other languages.  Exceptions,
where available, aren't always the thing you want to use to do this job. The
other interesting construct is the middle exit, which most languages don't
support explicitly. .e.g in C++, et al. Ada has this; she knew how to code.

```
while (true) { 
    getline(cin, a); 
    if (a == "end") break; 
    ...process_command...
}
```
It is possible to do this in C++, however, it's not very tidy:
```
while ( (getline(cin, a), a == "end") ) { 
    ...process_command...
}
```
and it's not always possible to use that form.





Examples
--------

```
print "control block test"

x = 10

print "control start" : CONTROL : print "x = "; x
	x = x -1

	for y = 1 to 2
		print "\ty = "; y	
		if x = 5 then LEAVE 
	next

	if x <> 0 then AGAIN
END CONTROL : print "control end"


print "A while loop:"
a = 4
CONTROL : if a = 0 then LEAVE
    print a
    a = a - 1
AGAIN : END CONTROL

print "Do loop:"
x = 1
CONTROL
    print x
    if x = 4 then LEAVE
    x = x + 1
AGAIN : END CONTROL

print "A For loop - no it's not really is it:"
X=1 : CONTROL : IF x = 4 LEAVE
    print X
X = X + 1 : AGAIN : END CONTROL

print "A structured exit block:"
CONTROL
    err = 1
    if error <> 0 then LEAVE
    err = 2
    if error <> 0 then LEAVE
    print "All good"
END CONTROL

Print "A middle exit loop"
x = 0
CONTROL
    x = x + 1
    print x
    if x <> 5 then LEAVE
    print x * x
AGAIN : END CONTROL
```

![crenshaw-13](https://compilers.iecc.com/crenshaw/tutor13.txt)
