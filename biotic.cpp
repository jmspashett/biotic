#include <algorithm>
#include <cassert>
#include <cctype>
#include <deque>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <memory>
#include <vector>

using namespace std;

enum class LexType {
    any=0, unknown=-0,
    str, number,
    identifier,
    keyword,
    op,
};

enum class AbstractType {
    any=0, unknown=0, str, number, real,
    label,
    identifier,
    op,
    function
};

struct LexToken
{
    LexType type;
    string lexeme;
};

using LexTokens = deque<LexToken>;


struct AbstractToken
{
    AbstractType type;
    string codeExpr;
};


struct TranspilerOptions
{
	struct
	{
		bool lexTokens;
        bool codeGen;
	} log;
	struct
	{
		bool suppressUnusedLabel;
	} hacks;
	struct
	{
		bool tempStringEllision;
	} optimisations;
    struct
    {
        bool reportMemoryAllocation;
    } runtime;
	string inputFile;
	string outputFile;
};


struct TranspilerContext
{
    string compilerName;

    // For lexical analysis
    string line; // Current source line
    size_t lineNumber {};
    LexTokens tokens;

    ifstream in;
    ofstream out;

    // Symbol table
    unordered_map<string, string> symbols;
    set<string> defined_functions;

    // Basic data array for the DATA statement
    vector<AbstractToken> data;

    // For code generation
    deque<AbstractToken> controlStack;
    stringstream functions;
    stringstream body;
    int scopeLevel {};
    int labelId {};
    struct {
        bool arrayUsed;
        bool seenGosub;
        bool sleepUsed;
        bool rndFunctionUsed;
        bool seenGet;
        bool seenPowerOp;
    } saturations {};
    //

    // All, and varied transpiler option "basket"
    TranspilerOptions transpilerOptions {};

    // What was found during compilation. - For exit code.
    struct  {
    	bool warn;
    	bool error;
    } detections {};

};

/* This stucture holds target labels for control structures,
 * currently only used by the CONTROL keyword
 */
struct Control
{
	AbstractToken againLabel;
	AbstractToken exitLabel;
};


// Forwards
void lex_dumpTokens(const LexTokens & tokens,
    const char * const additional_msg = nullptr);
void transpiler_error(TranspilerContext & context, const string message);
bool lex_line(TranspilerContext & context);
void parse_statement(TranspilerContext & context, Control control);
void transpiler_warn(TranspilerContext & context, const string message);
bool parse_variable(TranspilerContext & context, AbstractToken &);
bool parse_expression(TranspilerContext & context, AbstractToken & expr);
void transpiler_fatal(const TranspilerContext & context,
    const string & message);
void codeGen_putOut(TranspilerContext & context, const string & c_statement);
void codeGen_putOut(TranspilerContext & context,
    const stringstream & c_statement);
AbstractToken codeGen_nameToVar(const string & name, bool array);


/*
 * Utilities
 */
AbstractToken pop(TranspilerContext & context, deque<AbstractToken> & stack)
{
    if (stack.empty())
        transpiler_fatal(context, "Stack empty");
    const auto at(stack.back());
    stack.pop_back();
    return at;
}


string lowerCase(const std::string & str)
{
    string lower;
    std::transform(str.begin(), str.end(),
        back_inserter(lower), ::tolower);
    return lower;
}


void skipWs(string::const_iterator & i, const std::string & line)
{
    for (; i != cend(line); ++i ) {
        if (!isspace(*i))
            break;
    }
}


bool tok_empty(const LexTokens & tokens)
{
    return tokens.empty();
}


bool tok_peek(const LexTokens & tokens, LexToken & token, 
    LexType type = LexType::any, size_t at = 0)
{
    if (tokens.size() < at + 1)
        return false;
    token = tokens[at];
    if (type != LexType::any && token.type != type)
        return false;
    return true;
}


bool tok_match(const LexTokens & tokens, const char * what, 
    LexType type = LexType::any, bool ignoreCase = false, size_t at = 0)
{
    LexToken t;
    if (tok_peek(tokens, t, type, at)) {
        string lex;
        if (ignoreCase)
            lex = lowerCase(t.lexeme);
        else
            lex = t.lexeme;
        if (lex == what) {
            return true;
        }
    }
    return false;
}


bool tok_drop(LexTokens & tokens)
{
    if (tok_empty(tokens))
        return false;
    tokens.pop_front();
    return true;
}


bool tok_consume(LexTokens & tokens, const char * what,
    LexType type = LexType::any, bool ignoreCase = true)
{
    bool result = tok_match(tokens, what, type, ignoreCase);
    if (result)
        tok_drop(tokens);
    return result;
}


bool tok_next(LexTokens & tokens, LexToken & token, 
    LexType type = LexType::any)
{
    if ( tok_empty(tokens) )
        return false;
    token = tokens.front();
    if (type != LexType::any && token.type != type)
        return false;
    tokens.pop_front();
    return true;
}


void codeGen_openScope(TranspilerContext & context)
{
    codeGen_putOut(context, "{");
    ++context.scopeLevel;
}


void codeGen_closeScope(TranspilerContext & context)
{
    if (context.scopeLevel < 1)
        transpiler_fatal(context, "codeGen: no scopes to close");
    --context.scopeLevel;
    codeGen_putOut(context, "}");
}


void codeGen_closeAllScopes(TranspilerContext & context)
{
    while (context.scopeLevel) {
        codeGen_closeScope(context);
    }
}


AbstractToken codeGen_dimensionArray(TranspilerContext & context, 
    const AbstractToken & basicType, const deque<AbstractToken> & dims)
{
    context.saturations.arrayUsed = true;
    // Generate a definition for an array
    stringstream def;
    def << "struct array_descriptor " << basicType.codeExpr 
        << " = {NULL, NULL};";

    // Dimension information for runtime verification
    stringstream s;
    const string specName = basicType.codeExpr + "_spec";
    s << "size_t " << specName << "_dims[] = " << "{";
    for (const auto & d : dims) {
        s << "(size_t)(" << d.codeExpr << " + 1), ";
    }
    s << "};";
    codeGen_putOut(context, s);

    // A spec structure containing dimensions, and pointer to data
    // This is output in the code stream.
    s = stringstream();
    s << "struct array_spec " << specName << " = {";
    s << dims.size() << ", " << specName << "_dims, ";
   
    // Output the size of the basic element into the array_spec
    s << "sizeof " << basicType.codeExpr << ".data->";
    if (basicType.type == AbstractType::str)
        s << "str";
    else if (basicType.type == AbstractType::real)
        s << "real";
    else
        transpiler_fatal(context, "symbol_defineVariable: unsupported type");
    s << "};";
    codeGen_putOut(context, s);

    // Assign the spec structure to the array structure
    s = stringstream();
    s << basicType.codeExpr << ".spec = &" << specName << ";"; 
    codeGen_putOut(context, s);

    // At runtime, allocate the array
    s = stringstream();
    s << "dimension_array(&" << basicType.codeExpr << ");";
    codeGen_putOut(context, s);
    return AbstractToken { basicType.type, def.str() };
}


void codeGen_indent(TranspilerContext & context)
{
    for (auto n = 0; n <= context.scopeLevel; ++n)
        context.body << "\t";
}


void codeGen_putOut(TranspilerContext & context, const string & c_statement)
{
    if (!c_statement.empty()) {
        const string::size_type len = c_statement.length();
        const bool labelEnd = (len > 2) && 
            c_statement.compare(len - 2, string::npos, ":;") == 0;
        if (!labelEnd)
            codeGen_indent(context);
    }
    if (context.transpilerOptions.log.codeGen)
        clog << "code: " << c_statement << "\n";
    context.body << c_statement << "\n";
}


void codeGen_putOut(TranspilerContext & context,
    const stringstream & c_statement)
{
    codeGen_putOut(context, c_statement.str().c_str());
}


string codeGen_temp_str(TranspilerContext & context)
{
	codeGen_openScope(context);
    const string tempName = "temp" + to_string(context.scopeLevel);
    codeGen_putOut(context, "char " + tempName + "[256];");
    return tempName;
}


void codeGen_makeVarDefinitions(TranspilerContext & context, ostream & codeOut)
{
    for (const auto & d : context.symbols) {
        if (context.saturations.seenGosub)
            codeOut << "volatile ";
        codeOut << d.second << "\n";
    }
}


string directoryName(const string & file)
{
    const auto slashPos = file.find_last_of("/");
    return file.substr(0, slashPos + 1);
}


void codeGen_include(TranspilerContext & context, const string & fileName, 
    ostream & codeOut)
{
    const auto dir = directoryName(context.compilerName) + "runtime/";
    const auto includeFile = dir + fileName.c_str();
    ifstream file(includeFile);
    //file.exceptions(ios::failbit | ios::badbit);
    codeOut << file.rdbuf();
    if (!file.good())
        transpiler_fatal(context, "could not read include: " + includeFile);
}


void codeGen_data(TranspilerContext & context, ostream & codeOut)
{
    if (context.data.empty())
        return;
    codeGen_include(context, "data.h", codeOut);
    stringstream s;
    s << "const struct data_def data_array[] = {";
    for (const auto & x : context.data) {
        s << "{" << static_cast<int>(x.type) << ", ";
        s << "{";
        if (x.type == AbstractType::str) {
            s << ".str=" << x.codeExpr;
        }
        else if (x.type == AbstractType::real)
            s << ".real=" << x.codeExpr;
        s << "}";
        s << "}, ";
    }
    s << "};\n";
    s << "const size_t data_len = sizeof data_array / sizeof data_array[0];\n";
    codeOut << s.str();
    codeGen_include(context, "data.c", codeOut);
}


void codeGen_makeProlog(TranspilerContext & context, ostream & codeOut)
{
    codeOut <<
R"tag(
/* 
 * Generated by the Biotic Basic to C Transpiler
 * Biotic
 *  - Basic Input, Output To Intermediate C
 *  - Basic Input Output; Transpiling Is Compiling
 *
 * This file was originally generated by the Biotic transpiler, and therefore
 * is a function of the input source, as such the license of the source file
 * should be consulted. 
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <setjmp.h>

)tag";

    if (context.saturations.arrayUsed) {
        codeGen_include(context, "array.c", codeOut);  
    }
    if (context.saturations.sleepUsed) {
        codeGen_include(context, "sleep.c", codeOut);  
    }
    codeGen_include(context, "echo.c", codeOut);  
    if (context.saturations.seenGet)
        codeGen_include(context, "get.c", codeOut);
    if (context.saturations.rndFunctionUsed) {
        codeGen_include(context, "rnd.c", codeOut);
    }
    if (context.saturations.seenGosub) {
        codeGen_include(context, "gosub.c", codeOut);
    }
    if (context.saturations.seenPowerOp) {
        codeOut << "#include <math.h>\n";
    }
    codeGen_data(context, codeOut);
    codeOut << "\n";
    codeGen_makeVarDefinitions(context, codeOut);
    codeOut << "\n";
    codeOut << context.functions.str();
    codeOut << 
R"tag(
int main(void) 
{
    init_term();
)tag";
    codeOut << "\tsrand(time(NULL));\n";
    if (context.saturations.arrayUsed) {
        codeOut << "\treportMemoryAllocation = " 
            << (context.transpilerOptions.runtime.reportMemoryAllocation 
                ? "1" 
                : "0")
            << ";\n";
    }
}


void codeGen_makeEpilog(TranspilerContext & context, ostream & codeOut)
{
    codeOut << ";\n";
    codeOut << "}\n";
}


void codeGen_label(TranspilerContext & context, AbstractToken label)
{
	if (context.transpilerOptions.hacks.suppressUnusedLabel) {
        /* Hacks: suppress unreferenced label warning */
		codeGen_putOut(context, "goto " + label.codeExpr + ";");
    }
    // Output ; at the end of label to ensure a full statement
    codeGen_putOut(context, (label.codeExpr + ":;").c_str() );
}


AbstractToken codeGen_uniqueLabelName(TranspilerContext & context)
{
    const auto uniqueId = "label_U" + to_string(context.labelId++);
	const AbstractToken label {
		AbstractType::label,
		uniqueId
	};
    return label;
}


AbstractToken codeGen_uniqueLabel(TranspilerContext & context)
{
    const AbstractToken label = codeGen_uniqueLabelName(context);
    codeGen_label(context, label);
    return label;
}

AbstractToken codeGen_labelForId(TranspilerContext & context,
    const string & id)
{
    const AbstractToken label {
        AbstractType::label,
        "label_" + id
    };
    codeGen_label(context, label);
    return label;
}

// This is a magic var for ZX81
AbstractToken codeGen_random(TranspilerContext & context)
{
	return AbstractToken {
		AbstractType::real,
		"((float)rand() / RAND_MAX)"
	};
}

// This is for C64 (and some others no doubt)
AbstractToken codeGen_randomFunction(TranspilerContext & context, 
    const AbstractToken & expr)
{
    context.saturations.rndFunctionUsed = true;
	return AbstractToken {
		AbstractType::real,
		string("rnd(") + expr.codeExpr + ")"
	};
}


void codeGen_seed(TranspilerContext & context, const AbstractToken & seedValue)
{
	stringstream s;
	s << "srand((int)";
	if (seedValue.type == AbstractType::real)
		s << seedValue.codeExpr;
	else
		s << "1";
	s << ");";
	codeGen_putOut(context, s);
}


AbstractType parse_varType(const std::string & id)
{
    const auto type = id.find("$") != string::npos ? AbstractType::str :
        id.find("%") != string::npos ? AbstractType::number :
        AbstractType::real;
    return type;
}


AbstractToken codeGen_nameToVar(const LexToken & lex, bool array)
{
    const AbstractType type = parse_varType(lex.lexeme);
    string bareName;
    copy_if(begin(lex.lexeme), end(lex.lexeme), back_inserter(bareName),
        ::isalnum);
	string var = bareName;
	if ( type == AbstractType::str )
		var += "_str";
	else if (type == AbstractType::number )
		var += "_number"; 
	else
		var += "_real";
	if (array)
		var += "_array";
	return AbstractToken { type, var };
}


AbstractToken codeGen_numericOp(
    TranspilerContext & context,
    const AbstractToken & lhs,
    const AbstractToken & op,
    const AbstractToken & rhs)
{
    stringstream s;
    if (op.codeExpr == "^") {
        context.saturations.seenPowerOp = true;
        s << "pow(" << lhs.codeExpr << ", " << rhs.codeExpr << ')';
    } else if (op.codeExpr == "mod") {
        s << "fmod(" << lhs.codeExpr << ", " << rhs.codeExpr << ')';
    } else {
        s << lhs.codeExpr << ' ' << op.codeExpr << ' ' << rhs.codeExpr;
    }
    return AbstractToken { AbstractType::real, s.str() };
}


AbstractToken codeGen_binaryOp(
    TranspilerContext & context,
    const AbstractToken & lhs,
    const AbstractToken & op,
    const AbstractToken & rhs)
{
    if (lhs.type == AbstractType::real) {
        return codeGen_numericOp(context, lhs, op, rhs);
    } else if (lhs.type == AbstractType::str) {
        stringstream s;
        if (op.codeExpr == "+") {
            const auto id = codeGen_temp_str(context);
            s << "strcpy( " << id << ", " << lhs.codeExpr << ");";
            codeGen_putOut(context, s);
            s = stringstream();
            s << "strcat( " << id << ", " << rhs.codeExpr << ");";
            codeGen_putOut(context, s);
            return AbstractToken { AbstractType::str, id };
        } else
            transpiler_fatal(context, "parse_term: unimplemented operator");
    } else {
        transpiler_fatal(context, "parse_term: unknown LHS type");
    }
    abort();
}


AbstractToken codeGen_comparison(
    TranspilerContext & context,
    const AbstractToken & left,
    const AbstractToken & op,
    const AbstractToken & rhs)
{
    if (left.type == AbstractType::real) {
        return AbstractToken {
            left.type,
            left.codeExpr + ' ' + op.codeExpr + ' ' + rhs.codeExpr
        };
    } else if (left.type == AbstractType::str) {
        stringstream s;
        s << "(strcmp( " << left.codeExpr << ", "
            << rhs.codeExpr << ") "
            << op.codeExpr << " 0)";
        return AbstractToken { AbstractType::real, s.str() };
    } else {
        transpiler_fatal(context, "comaprison: unsupported type");
        return AbstractToken {};
    }
}


void codeGen_print(TranspilerContext & context, const stringstream & fmt, 
    const bool emitEol, const deque<AbstractToken> & eList)
{
    stringstream s;
    s << "printf(\"" << fmt.str();
    if (emitEol)
        s << "\\n";
    s << '"';
    for (const auto e : eList) {
        s << ", ";
        if (e.type == AbstractType::real)
            s << "(float)(";
        s << e.codeExpr;
        if (e.type == AbstractType::real)
            s << ")";
    }
    s << ");\n";
    codeGen_putOut(context, s);
    if (!emitEol)
        codeGen_putOut(context, "fflush(stdout);");
}


void codeGen_inputStatement(TranspilerContext & context, 
    const AbstractToken & prompt, const vector<AbstractToken> & variables)
{
    if (prompt.type == AbstractType::str) {
        codeGen_putOut(context, "printf(\"%s\", " + prompt.codeExpr + ");");
    } 
    codeGen_putOut(context, "echo(1);");
    auto i = variables.cbegin();
    if (i != variables.cend()) {
        codeGen_putOut(context, "printf(\"? \");");
        for (;;)  {
            const auto & v = *i;
            if (v.type == AbstractType::str) {
                stringstream s;
                s << "fgets( " << v.codeExpr << ", sizeof " << v.codeExpr
                    << ", stdin);";
                codeGen_putOut(context, s);
                s = stringstream();
                codeGen_openScope(context);
                s << "char * const lf = strchr(" << v.codeExpr << ", '\\n');"
                    << "if (lf) *lf = '\\0';";
                codeGen_putOut(context, s);
                codeGen_closeScope(context);
            } else if (v.type == AbstractType::real) {
                stringstream s;
                const string tempName = codeGen_temp_str(context);
                s << "fgets(" << tempName << ", sizeof " << tempName 
                    << ", stdin);";
                codeGen_putOut(context, s);
                s = stringstream();
                s << "sscanf(" << tempName << ", \"%f\", &" << v.codeExpr 
                    << ");";
                codeGen_putOut(context, s);
            }
            ++i;
            if (i == variables.cend())
                break;
            codeGen_putOut(context, "printf(\"?? \");");
        }
    }
    codeGen_putOut(context, "echo(0);");
}


AbstractToken codeGen_chr(TranspilerContext & context,
    const AbstractToken & expr)
{
    const auto id = codeGen_temp_str(context);
    stringstream s;
    s << id << "[1] = 0; " << id << "[0] = " << expr.codeExpr << ";";
    codeGen_putOut(context, s);
    return AbstractToken{ AbstractType::str, id};
}


string codeGen_typeCastTo(AbstractType t, bool ptr)
{
    stringstream code;
    code << "(";
    switch (t) {
    case AbstractType::real:
        code << "float";
        break;
    case AbstractType::number:
        code << "int";
        break;
    case AbstractType::str:
        code << "char";
        break;
    default:
        cerr << "AbstractType conversion not implemented\n";
        abort();
        break;
    }
    if (ptr)
        code << " *";
    code << ")";
    return code.str();
}


AbstractToken codeGen_typeConv(TranspilerContext & context,
    const AbstractToken & expr, AbstractType targetType)
{
    stringstream i;
    i << codeGen_typeCastTo(targetType, false) << '(' << expr.codeExpr << ')';
    if (targetType == AbstractType::number) // TODO numbers are reals, for now
        targetType = AbstractType::real;
    return AbstractToken { targetType, i.str() };
}


void codeGen_assignment(TranspilerContext & context, 
    const AbstractToken & left, const AbstractToken & right)
{
    if (left.type == AbstractType::str) {
        stringstream s;
        s << "strcpy( " << left.codeExpr << ", " << right.codeExpr << ");";
        codeGen_putOut(context, s);
    } else if (left.type == AbstractType::real) {
        stringstream s;
        s << left.codeExpr << " = " << right.codeExpr << ";";
        codeGen_putOut(context, s);
    } else {
        transpiler_fatal(context, "assignment: LHS has unknown type");
    }
}


AbstractToken codeGen_next(TranspilerContext & context)
{
    const auto endExpr = pop(context, context.controlStack);
    const auto label = pop(context, context.controlStack);
    const auto step = pop(context, context.controlStack);
    const auto loopVar = pop(context, context.controlStack);
    stringstream s;
    s << "if (" << loopVar.codeExpr << " < " << endExpr.codeExpr 
        << ")";
    codeGen_putOut(context, s);
    s = stringstream();
    codeGen_openScope(context);
    s << loopVar.codeExpr << " += " << step.codeExpr << ";";
    codeGen_putOut(context, s);
    s = stringstream();
    s << "goto " + label.codeExpr << ";";
    codeGen_putOut(context, s);
    codeGen_closeScope(context);
    return loopVar;
}


void codeGen_goto(TranspilerContext & context, const AbstractToken & target)
{
    stringstream s;
    s << "goto " << target.codeExpr << ";";
    codeGen_putOut(context, s);
}


void codeGen_gosub(TranspilerContext & context, const AbstractToken & target)
{
    context.saturations.seenGosub = true;
    codeGen_putOut(context, "if (jmp_index == 0)");
    context.body << "\t";
    codeGen_putOut(context, "stack_overflow();");
    codeGen_putOut(context, "if (setjmp(jmp_bufs[--jmp_index]) == 0)");
    context.body << "\t";
    codeGen_goto(context, target);
}


void codeGen_return(TranspilerContext & context)
{
    codeGen_putOut(context, "longjmp(jmp_bufs[jmp_index++], 1);");
}


void codeGen_if(TranspilerContext & context, const AbstractToken & expr)
{
    stringstream s;
    s << "if ( " << expr.codeExpr << " ) ";
    codeGen_putOut(context, s);
    codeGen_openScope(context);
}


AbstractToken codeGen_cFunction(TranspilerContext & context, const char * name, 
    AbstractType returnType)
{
    stringstream s;
    s << name << "()";
    return AbstractToken { returnType, s.str() };
}


AbstractToken codeGen_factor(TranspilerContext & context,
    const AbstractToken & op, const AbstractToken & rvalue)
{
    AbstractToken ret {
        rvalue.type,
        op.codeExpr + rvalue.codeExpr
    };
    return ret;
}


AbstractToken codeGen_fn(TranspilerContext & context, const LexToken & fName, 
    AbstractToken & arg)
{
    stringstream s;
    // TODO constant
    s << "def_fn_" << fName.lexeme << '(' << arg.codeExpr << ')';
    return AbstractToken { AbstractType::real, s.str()};
}


AbstractToken codeGen_array(TranspilerContext & context,
    const AbstractToken & basicType, const deque<AbstractToken> & dims)
{
    stringstream s;
    if (basicType.type != AbstractType::str)
        s << '*';
    const bool ptr(true);
    s << codeGen_typeCastTo(basicType.type, ptr);
    s << "array_access(&" << basicType.codeExpr << ", " << dims.size();
    for (const auto & e : dims) {
        s << ", " << "(size_t)(" << e.codeExpr << ')';
    }
    s << ')';
    return AbstractToken { basicType.type, s.str() };
}


AbstractToken codeGen_declareVar(TranspilerContext & context, 
    const AbstractToken & var)
{
    AbstractToken decl(var);
    stringstream def;
    if ( var.type == AbstractType::str )
        def << "char " << var.codeExpr << "[255] = {0};";
    else if (var.type == AbstractType::number )
        def << "short " << var.codeExpr << " = 0;";
    else if (var.type == AbstractType::real)
        def << "float " << var.codeExpr << " = 0.0;";
    else
        transpiler_fatal(context, "symbol_defineVariable: unknown type");
    decl.codeExpr = def.str();
    return decl;
}


deque<AbstractToken> defaultDims(deque<AbstractToken> dims)
{
    transform(begin(dims), end(dims), begin(dims), 
        [](auto & t) {
            return AbstractToken { t.type, "10" };
        });
    return dims;
}


void symbol_defineVariable(
    TranspilerContext & context,
    const string & id,
    const AbstractToken & basicType,
    const deque<AbstractToken> & dims,
    const bool definition)
{
    const bool foundSymbol = context.symbols.find(id) != end(context.symbols);
    if (definition && foundSymbol) {
        transpiler_error(context, "redefining '" + id + "' not allowed");
        return;
    }
    if (foundSymbol)
        return;
    const auto array = !dims.empty();
    AbstractToken def;
    if (array) {
        def = codeGen_dimensionArray(context, basicType, 
            definition ? dims : defaultDims(dims));
    } else {
        def = codeGen_declareVar(context, basicType);
    }
    context.symbols.insert({id, def.codeExpr});
}


void parse_dumpControlStack(const deque<AbstractToken> & stack, 
    const char * const name = nullptr)
{
    if (name)
        clog << name << ": ";
    clog << "[";
    auto i = stack.cbegin();
    if ( i != cend(stack)) {
    	const int type = static_cast<int>(i->type);
        clog << "(type: " << type << ", " << "id: '" <<  i->codeExpr << "')";
        for (++i; i != cend(stack); ++i) {
            clog << ", (type: " << type << ", "
                << "id: '" <<  i->codeExpr << "')";
        }
    }
    clog << "]\n";
}


void parse_lineNumber(TranspilerContext & context)
{
    LexToken tok;
    if (!tok_peek(context.tokens, tok))
        return;
    const auto end = crbegin(tok.lexeme);
    const bool label = end != crend(tok.lexeme) && *end == ':';
    if (tok.type == LexType::number || label) {
    	const string id =
    			label
				? tok.lexeme.substr(0, tok.lexeme.size() - 1)
				: tok.lexeme;
    	const AbstractToken a = codeGen_labelForId(context, id);
        tok_drop(context.tokens);
    } 
}


bool parse_dynamicVar(TranspilerContext & context, AbstractToken & var)
{
    if (tok_consume(context.tokens, "rnd")) {
    	var = codeGen_random(context);
        return true;
    } else if (tok_consume(context.tokens, "get$")) {
        context.saturations.seenGet = true;
        var = codeGen_cFunction(context, "get_str", AbstractType::str);
        return true;
    } else if (tok_consume(context.tokens, "get")) {
        context.saturations.seenGet = true;
        var = codeGen_cFunction(context, "get_real", AbstractType::real);
        return true;
    } 
    return false;
}


AbstractToken parse_func_arg1(TranspilerContext & context, AbstractType type)
{
    AbstractToken arg = { AbstractType::unknown };
    if (!tok_consume(context.tokens, "(")) {
        transpiler_error(context, 
            "function arg: opening parenthesis missing");
        return arg;
    }
    AbstractToken expr;
    if (!parse_expression(context, expr)) {
        transpiler_error(context, 
            "function arg: expecting expression after (");
        return arg;
    }
    if (type != AbstractType::any && expr.type != type) {
        transpiler_error(context, "function arg: wrong type of argument");
        return arg;
    }
    if (!tok_consume(context.tokens, ")")) {
        transpiler_error(context, 
            "function arg: closing parenthesis missing");
        return arg;
    }
    return expr;
}

bool parse_func(TranspilerContext & context, AbstractToken & func)
{
    if (tok_consume(context.tokens, "chr$")) {
        AbstractToken expr = parse_func_arg1(context, AbstractType::real);
        func = codeGen_chr(context, expr); 
        return true;
    } else if (tok_consume(context.tokens, "int")) {
        AbstractToken expr = parse_func_arg1(context, AbstractType::real);
        func = codeGen_typeConv(context, expr, AbstractType::number);
        return true;
    } else if (tok_match(context.tokens, "(", LexType::any, false, 1) && 
        tok_consume(context.tokens, "rnd")
    ) {
        AbstractToken expr = parse_func_arg1(context, AbstractType::real);
        func = codeGen_randomFunction(context, expr);
        return true;
    }
    return false;
}


bool parse_op(TranspilerContext & context, AbstractToken & op,
    const set<string> & which)
{
	LexToken tok;
	if (tok_peek(context.tokens, tok, LexType::op)) {
        const auto i = which.find(tok.lexeme);
        if (i != cend(which)) {
            op = AbstractToken { AbstractType::op, tok.lexeme };
            tok_drop(context.tokens);
			return true;
		}
	}
	return false;
}


bool parse_mulOrDiv(TranspilerContext & context, AbstractToken & op)
{
    // Group 5 C++ operator precedence, what would one call this group?
	return parse_op(context, op, {"*", "/", "mod", "^"});
}


bool parse_addOrSub(TranspilerContext & context, AbstractToken & op)
{
	return parse_op(context, op, {"+", "-"});
}


bool parse_relop(TranspilerContext & context, AbstractToken & op)
{
    LexToken tok;
    if (!tok_peek(context.tokens, tok)) 
        return false;
    const string & l = tok.lexeme;
    if (l == "<" || l == ">" || l == "<=" || l == ">=" ) {
        op = AbstractToken { AbstractType::op, l };
    } else if (l == "<>") {
        op = AbstractToken { AbstractType::op, "!=" };
    } else if (l == "=") {
        op = AbstractToken { AbstractType::op, "==" };
    } else if (l == "or") {
        op = AbstractToken { AbstractType::op, "||" };
    } else {
        return false;
    }
    tok_drop(context.tokens);
	return true;
}


bool parse_literal(TranspilerContext & context, AbstractToken & literal)
{
    LexToken t;
    if (!tok_peek(context.tokens, t))
        return false;
    if (t.type == LexType::str || t.type == LexType::number) { // Literal
        // TODO: Well it's probably a real unless,
        // it's not and it's a number
        literal.type = t.type == LexType::str 
            ? AbstractType::str 
            : AbstractType::real;
        literal.codeExpr = t.lexeme;
        tok_drop(context.tokens);
        return true;
    }
    return false;
}


bool parse_subExpression(TranspilerContext & context, AbstractToken & expr)
{
    if (!tok_consume(context.tokens,"(")) // Sub-Expression
        return false;
    if (!parse_expression(context, expr))
        transpiler_error(context, "subExpression: expecting expression "
            "within brackets");
    if (!tok_consume(context.tokens, ")"))
        transpiler_error(context, "subExpression: expecting close bracket");
    stringstream s;
    s << " ( " << expr.codeExpr << " ) ";
    expr.codeExpr = s.str(); // Pass though the expression
    return true;
}


bool parse_fn(TranspilerContext & context, AbstractToken & rvalue)
{
    if (!tok_consume(context.tokens, "fn"))
        return false;
    LexToken fName;
    if (!tok_next(context.tokens, fName, LexType::identifier)) {
        transpiler_error(context, "fn: expecting funciton name");
        return false;
    }
    if (context.defined_functions.find(fName.lexeme) ==
        cend(context.defined_functions)
    ) {
        transpiler_error(context, "fn: undefined funciton: " + fName.lexeme);
        return false;
    }
    AbstractToken arg = parse_func_arg1(context, AbstractType::real);
    if (arg.type == AbstractType::unknown) {
        transpiler_error(context, "fn: expecting real function parameter");
        return false;
    }
    rvalue = codeGen_fn(context, fName, arg);
    return true;
}


bool parse_factor(TranspilerContext & context, AbstractToken & factor)
{
    if (tok_empty(context.tokens))
    	return false;
    
    // Find unary operator, if any
    AbstractToken op;
    parse_addOrSub(context, op);

    AbstractToken rvalue;
    do {
        if (parse_fn(context, rvalue))
            break;
        if (parse_func(context, rvalue))
            break; 
        else if (parse_variable(context, rvalue))
            break;
        else if (parse_literal(context, rvalue)) {
            break;
        } else if (parse_subExpression(context, rvalue)) {
            break;
        }
        return false;
    } while (false);
    factor = codeGen_factor(context, op, rvalue);
    return true;
}


bool parse_term(TranspilerContext & context, AbstractToken & term)
{
    if (parse_factor(context, term)) {
        AbstractToken op;
        while (parse_mulOrDiv(context, op)) {
            AbstractToken rhs;
            if (!parse_factor(context, rhs)) 
                transpiler_error(context, 
                    "term: factor expected after operator");
            term = codeGen_binaryOp(context, term, op, rhs);
        }
        return true;
    }
    return false;
}


bool parse_arithmeticExpression(TranspilerContext & context,
    AbstractToken & mathExpr)
{
    if (parse_term(context, mathExpr)) {
        // Term collection
        AbstractToken op;
        while (parse_addOrSub(context, op)) {
            AbstractToken rhs;
            if (!parse_term(context, rhs))
                transpiler_error(context, 
                    "arithmetic expression: expecting RHS");
            mathExpr = codeGen_binaryOp(context, mathExpr, op, rhs);
        }
        return true; 
    }
    return false;
}


bool parse_expression(TranspilerContext & context, AbstractToken & expr)
{
    if (parse_arithmeticExpression(context, expr)) {
        // Relop
        AbstractToken comp;
        while (parse_relop(context, comp)) {
            AbstractToken rhs;
            if (!parse_arithmeticExpression(context, rhs))
                transpiler_error(context, 
                    "expression: expecting expr on RHS "
                    "of relational operator");
            if (expr.type != rhs.type)
            	transpiler_error(context, "LHS and RHS in comparison differ "
                    "in type");
            expr = codeGen_comparison(context, expr, comp, rhs);
        }
        return true;
    }
    return false;
}


AbstractToken parse_array(TranspilerContext & context, string & id,
    deque<AbstractToken> & dims, const AbstractToken & basicType )
{
    id += "[]";
    AbstractToken e;
    while (parse_expression(context, e)) {
        dims.push_back(e);
        tok_consume(context.tokens, ",");
    }
    if (!tok_consume(context.tokens, ")"))
        transpiler_error(context, "array: missing ')'");
    return codeGen_array(context, basicType, dims);
}


bool parse_basic_variable(TranspilerContext & context, 
    AbstractToken & variable, bool definition = false)
{
    LexToken lex;
    if (!tok_next(context.tokens, lex, LexType::identifier))
        return false;
    const bool array = tok_consume(context.tokens, "(");
    const AbstractToken basicType = codeGen_nameToVar(lex, array);
    deque<AbstractToken> dims;
    string id = lex.lexeme;
    if (array) {
        variable = parse_array(context, id, dims, basicType);
    } else {
        variable = basicType;
    }
    symbol_defineVariable(context, id, basicType, dims, definition);
    return true;
}

bool parse_variable(TranspilerContext & context, AbstractToken & variable)
{
    if (parse_dynamicVar(context, variable))
        return true;
    return parse_basic_variable(context, variable);
} 


void parse_printStatement(TranspilerContext & context)
{
    stringstream s;
    deque<AbstractToken> eList;
    stringstream fmt;
    bool emitEol = true;
    while (!tok_empty(context.tokens)) {
        AbstractToken expr;
        if (parse_expression(context, expr)) {
            emitEol = true;
            fmt <<  
                (expr.type == AbstractType::str
                ? "%s" :
                "%g"); // TODO it's a float, unless it's a number
            eList.push_back(expr);
        } else {
            const bool semiColon = tok_consume(context.tokens, ";");
            const bool comma = tok_consume(context.tokens, ",");
            const bool formatter = semiColon || comma;
            if (formatter) {
                emitEol = false;
                if (comma)
                    fmt << "\t";
                if (semiColon)
                    fmt << " ";
            } else {
                break;
            }
        }
    }
    codeGen_print(context, fmt, emitEol, eList);
}


void parse_inputStatement(TranspilerContext & context)
{
    AbstractToken prompt;
    if (parse_literal(context, prompt)) {
        if (prompt.type != AbstractType::str) {
            transpiler_error(context, 
                "Input statement prompt should be a string");
            return;
        }
        tok_consume(context.tokens, ";");
    }
    vector<AbstractToken> variables;
    do {
        AbstractToken v;
        if (!parse_variable(context, v))
            transpiler_error(context, "input_statement: expecting variable");
        variables.push_back(v);
    } while (tok_consume(context.tokens, ","));
    codeGen_inputStatement(context, prompt, variables);
}


bool parse_assignment(TranspilerContext & context, AbstractToken & left)
{
    if (!parse_variable(context, left)) {
        transpiler_error(context, "assignment: variable expression is"
            " required on LHS");
        return false;
    }
    if (!tok_consume(context.tokens, "=")) {
        transpiler_error(context, "assignment: expecting '='");
        return false;
    }
    AbstractToken right;
    if (!parse_expression(context, right)) {
        transpiler_error(context, "assignment: expecting rhs expression");
        return false;
    }
    if (left.type != right.type) {
        transpiler_error(context, "assignment: type mismatch LHS and RHS");
        return false;
    }
    codeGen_assignment(context, left, right);
    return true;
}


bool parse_label(TranspilerContext & context, AbstractToken & label)
{
    LexToken l;
    if (tok_next(context.tokens, l, LexType::number) || 
        tok_next(context.tokens, l, LexType::identifier)) 
    {
        label = { AbstractType::label, "label_" + l.lexeme };
        return true;
    }
    return false;
}


void parse_seed(TranspilerContext & context)
{
	AbstractToken e = { AbstractType::unknown };
	if (parse_expression(context, e)) {
		codeGen_seed(context, e);
	}
}


void parse_forStatement(TranspilerContext & context)
{
	AbstractToken loopVar;
	if (!parse_assignment(context, loopVar))
	    transpiler_error(context, "for: Need a loop control assignment");
	if (!tok_consume(context.tokens, "to"))
		transpiler_warn(context, "for: expecting TO keyword in for loop");
	AbstractToken endExpr;
	if (!parse_expression(context, endExpr))
		transpiler_error(context, "for: missing limit expression after TO");
	const AbstractToken step { AbstractType::real, "1" };
	context.controlStack.push_back(loopVar);
	context.controlStack.push_back(step);
	const AbstractToken srcLabel = codeGen_uniqueLabel(context);
	context.controlStack.push_back(srcLabel);
	context.controlStack.push_back(endExpr);
}


void parse_next(TranspilerContext & context)
{
    const auto loopVar = codeGen_next(context);
    AbstractToken nextVar;
    if (parse_variable(context, nextVar) && 
        nextVar.codeExpr != loopVar.codeExpr)
    {
    	transpiler_warn(context, "next: loop variable mismatch " +
    		loopVar.codeExpr + " vs " + nextVar.codeExpr);
    }
}


void parse_dim(TranspilerContext & context)
{
	AbstractToken variable;
    do {
        const bool definition = true;
        if (!parse_basic_variable(context, variable, definition))
            transpiler_error(context, "dim: expecting dimensioned variable");
    } while (tok_consume(context.tokens, ","));
}


void parse_goto(TranspilerContext & context)
{
    AbstractToken target;
    if (!parse_label(context, target))
        transpiler_error(context, "goto: expecting target line");
    codeGen_goto(context, target);
}


void parse_if(TranspilerContext & context, Control & control)
{
    AbstractToken expression;
    if (!parse_expression(context, expression))
        transpiler_error(context, "if: needs an expression");
    codeGen_if(context, expression);
    if (tok_consume(context.tokens, "then")) { // optional
        LexToken s;
        if (tok_peek(context.tokens, s, LexType::number)) {
            AbstractToken target;
            parse_label(context, target);
            codeGen_goto(context, target);
        }
    }
}


bool parse_controlEnd(TranspilerContext & context)
{
	return tok_consume(context.tokens, "control");
}


void parse_controlStatement(TranspilerContext & context)
{
	Control control = {
			codeGen_uniqueLabel(context),
            codeGen_uniqueLabelName(context)
	};
	parse_statement(context, control);
}


void parse_sleep(TranspilerContext & context)
{
    AbstractToken duration;
    if (parse_expression(context, duration)) {
        if (duration.type == AbstractType::real) {
            context.saturations.sleepUsed = true;
            stringstream s;
            s << "sleep_s(" << duration.codeExpr << ");"; 
            codeGen_putOut(context, s);
        } else {
            transpiler_error(context, "sleep: expecting real expression");
        }
    } else {
        transpiler_error(context, "sleep: expecting expression");
    }
}


void parse_data(TranspilerContext & context)
{
    AbstractToken l;
    // TODO - this was parse_literal, but it needs to be a compile time
    // expression
    while (parse_expression(context, l)) {
        context.data.push_back(l);
        if (!tok_consume(context.tokens, ","))
            break;
    }
}


void parse_read(TranspilerContext & context)
{
    stringstream s;
    AbstractToken left;
    while (parse_variable(context, left)) {
        const string sel { left.type == AbstractType::str ? "str" : "real" };
        AbstractToken right { 
            AbstractType::unknown, left.type == AbstractType::str 
            ? "read_str()" : "read_real()"};
        codeGen_assignment(context, left, right);
        if (!tok_consume(context.tokens, ","))
            break;
    }
}


void codeGen_def_fn(TranspilerContext & context, 
    const LexToken & fName, const AbstractToken & arg,
    const AbstractToken & expr)
{
    AbstractToken fName_at { AbstractType::function, 
        "def_fn_" + fName.lexeme 
    };
    stringstream & s = context.functions;
    s << "float " << fName_at.codeExpr << '(' << "float " << arg.codeExpr << ')' << '\n';
    s << "{\n";
    s << "\treturn " << expr.codeExpr << ";\n"; 
    s << "}\n";
    context.defined_functions.insert(fName.lexeme);
}


void parse_def(TranspilerContext & context)
{
    if (!tok_consume(context.tokens, "fn")) {
        transpiler_error(context, "def: expecting fn token");
        return;
    }
    LexToken fName;
    if (!tok_next(context.tokens, fName, LexType::identifier)) {
        transpiler_error(context, "def: expecting funciton name");
        return;
    }
    AbstractToken arg = parse_func_arg1(context, AbstractType::real);
    if (arg.type == AbstractType::unknown) {
        transpiler_error(context, "def: expecting function parameter");
        return;
    }
    if (!tok_consume(context.tokens, "=")) {
        transpiler_error(context, "def: expecting = after funcrion parameter");
        return;
    }
    AbstractToken expr;
    if (!parse_expression(context, expr)) {
        transpiler_error(context, "def: expression after =");
        return;
    }
    codeGen_def_fn(context, fName, arg, expr); 
}


void parse_statement(TranspilerContext & context, Control control)
{
    /*  This also allows if statements without a statement which, 
        curiously enough C64 basic also allows . This is partly due to 
        the way the grammar is defined for statements. In particular 
        because a newline starts the grammar afresh from a new statement.
    e.g. 
        if 5 then <nothing>
     */
    bool lex_ok;
    do {
        LexToken tok;
        while (tok_next(context.tokens, tok)) {
            const string & lex = tok.lexeme;
            if (lex == "rem") {
                codeGen_putOut(context, (";// " + context.line).c_str() );
                context.tokens.clear();
            } else if (lex == "print") {
                parse_printStatement(context);
            } else if (lex == "input") {
                parse_inputStatement(context);
            } else if (lex == "goto") {
                parse_goto(context);
            } else if (lex == "let") {
                AbstractToken lvalue;
                parse_assignment(context, lvalue);
            } else if (lex == "if") {
                parse_if(context, control);
            } else if (lex == "for") {
                parse_forStatement(context);
            } else if (lex == "next") {
                parse_next(context);
            } else if (lex == "end") {
                if (parse_controlEnd(context)) {
                    codeGen_label(context, control.exitLabel);
                    return;
                } else {
                    codeGen_putOut(context, "exit(0);");
                }
            } else if (lex == "stop") {
                // This should really call raise(SIGTRAP) or DebugBreak on
                // windows.
                codeGen_putOut(context, "exit(1);");
            } else if (lex == "seed") {
                parse_seed(context);
            } else if (lex == "dim") {
                parse_dim(context);
            } else if (lex == "control") {
                parse_controlStatement(context);
            } else if (lex == "leave") {
                codeGen_goto(context, control.exitLabel);
            } else if (lex == "again") {
                if (control.againLabel.codeExpr.empty()) {
                    transpiler_error(context, 
                        "again: requires prevoius control block");
                }
                codeGen_goto(context, control.againLabel);
            } else if (lex == "gosub") {
                AbstractToken target;
                if (!parse_label(context, target))
                    transpiler_error(context, 
                        "gosub: expecting target line");
                codeGen_gosub(context, target);
            } else if (lex == "return" ) {
                codeGen_return(context);
            } else if (lex == "cls" ) {
                codeGen_putOut(context, R"(printf("\033c");)");
            } else if (lex == "sleep") {
                parse_sleep(context);
            } else if (lex == "data") {
                parse_data(context);
            } else if (lex == "read") {
                parse_read(context);
            } else if (lex == "restore") {
                codeGen_putOut(context, "restore();");
            } else if (lex == "def") {
                parse_def(context);
            } else if (lex == ":") {
                /* 
                    Null statement terminates inner parsing
                    But does nothing at the top level
                */
            } else { // Otherwise it is an assignment
                context.tokens.push_front(tok);
                AbstractToken lvalue;
                if (!parse_assignment(context, lvalue))
                    tok_drop(context.tokens);
            }
        }
        codeGen_closeAllScopes(context);
        lex_ok = lex_line(context);
        parse_lineNumber(context);
    } while (lex_ok);
}


bool parse_stopDirective(LexTokens & tokens)
{
    if (!tok_consume(tokens, "#"))
        return false;
    return tok_consume(tokens, "stop");
}


/*
 * Lexical analysis
 */
string lex_string(
    TranspilerContext & context,
    string::const_iterator & i,
    const string::const_iterator & end) 
{
    string tok;
    tok += *i++;
    for (; i != end; ++i ) {
        tok += *i;
        if ( *i=='"' )
            break;
    }
    if ( *i != '"' )
        transpiler_error(context, "Unterminated string at EOL");
    else
        ++i;
    return tok;
}


string lex_number(string::const_iterator & i,
    const string::const_iterator & end)
{
    string tok;
    for (; i != end && 
        (isdigit(*i) || *i == '.' || tolower(*i) == 'e'); 
        ++i ) 
    {
        tok += *i;
    }
    return tok;
}


string lex_identifier(string::const_iterator & i, 
    const string::const_iterator & end)
{
    // FN is a special identifier that does not have to be followed by a space
    const char * fn = "fn";
    string tok;
    for (; *fn != '\0' &&
        ( isalpha(*i) || isdigit(*i) || *i == '$' || *i == ':' || *i == '_');
        ++i) 
    {
        tok += tolower(*i);
        tolower(*i) == *fn ? ++fn : fn;
    }
    return tok;
}


string lex_op(string::const_iterator & i, 
    const string::const_iterator & end)
{
    string tok;
    tok = *i++;
    if (i != end) {
        switch (tok[0]) {
            case '<':
                if (*i == '>') tok += *i++;
                break;
        }
    }
    return tok;
}


LexTokens lex_scan(TranspilerContext & context, const string & line)
{
    LexTokens tokens;
    auto i = line.cbegin();
    const auto end = line.cend();
    while (true) {
        skipWs(i, line);
        if ( i == end ) 
            break;
        string lexeme;
        LexType type = LexType::unknown;
        if ( *i=='"' ) {
            type = LexType::str;
            lexeme = lex_string(context, i, end);
        } else if (isdigit(*i)) {
            type = LexType::number;
            lexeme = lex_number(i, end); 
        } else if (isalpha(*i)) {
            type = LexType::identifier;
            lexeme = lex_identifier(i, end);
        } else if (ispunct(*i)) {
            type = LexType::op;
            lexeme = lex_op(i, end);
        } else {
            transpiler_error(context, ("unrecognised lexeme: " +
                string(i, end)).c_str());
        }
        if (lexeme == "mod") {
            type = LexType::op;
        }
        tokens.push_back({type, lexeme}); 
    }
    return tokens;
}


void lex_dumpTokens(const LexTokens & tokens,
    const char * const additional_msg)
{
    if (additional_msg)
        clog << additional_msg << ": ";
    clog << "tokens: [";
    auto i = tokens.cbegin();
    if ( i != cend(tokens) ) {
        clog << "'" << i->lexeme << "'";
        for (++i ; i != cend(tokens); ++i ) {
            clog << ", " << "'" << i->lexeme << "'";
        }
    }
    clog << "]\n";
}


bool lex_line(TranspilerContext & context)
{
    getline(context.in, context.line);
    if (!context.in.good())
        return false;
    context.body << "\t// " << context.line << "\n";
    context.tokens = lex_scan(context, context.line);
    if (parse_stopDirective(context.tokens))
        return false;
    if (context.transpilerOptions.log.lexTokens)
    	lex_dumpTokens(context.tokens);
    context.lineNumber++;
    return true;
}


void transpiler_message(const TranspilerContext & context, 
    const string & level, const string & message)
{
    cerr << level << ": at source line " << context.lineNumber << ": "
        << message <<"\n";
    cerr << "\tWith remaining ";
    lex_dumpTokens(context.tokens);
    cerr << "\tline: " << context.line << "\n";
}


void transpiler_fatal(const TranspilerContext & context,
    const string & message)
{
	transpiler_message(context, "FATAL", message);
    abort();
}


void transpiler_error(TranspilerContext & context, const string message)
{
	context.detections.error = true;
	transpiler_message(context, "ERROR", message);
}


void transpiler_warn(TranspilerContext & context, const string message)
{
	context.detections.warn = true;
	transpiler_message(context, "WARN", message);
}


enum class ReturnCode {
	Ok,
	Warnings,
	LogicErrors,
	Errors
};


ReturnCode transpiler_done(TranspilerContext & context)
{
	auto ret = ReturnCode::Ok;
    if (!context.controlStack.empty()) {
        transpiler_warn(context, "control stack isn't empty");
        parse_dumpControlStack(context.controlStack, "final control Stack");
        ret = ReturnCode::LogicErrors;
    }
    if (context.detections.warn)
    	ret = ReturnCode::Warnings;
    if (context.detections.error)
    	ret = ReturnCode::Errors;
    return ret;
}


void commandLineOptions(TranspilerContext & context, int argc, char * argv[])
{
    context.compilerName = argv[0];
    TranspilerOptions & options = context.transpilerOptions;
	size_t paramIndex = 0;
	while (*++argv) {
		const string arg(*argv);
		if (arg[0] == '+' || arg[0] == '-') {
			bool state = arg[0] == '+';
			const string opt(arg.substr(1));
			if (opt == "hacks.suppressUnusedLabel") 
				options.hacks.suppressUnusedLabel = state;
			else if (opt == "log.lexTokens")
				options.log.lexTokens = state;
            else if (opt == "log.codeGen")
                options.log.codeGen = state;
            else if (opt == "runtime.reportMemoryAllocation")
                options.runtime.reportMemoryAllocation = state;
			else {
				cerr << "unknown option: " << opt << "\n";
				throw runtime_error("unknown option");
			}
		} else {
			++paramIndex;
			switch (paramIndex) {
			case 1:
				options.inputFile = *argv;
				break;
			case 2:
				options.outputFile = *argv;
				break;
			}
		}
	}
}


int main(int argc, char * argv[] )
{
    TranspilerContext context;
	commandLineOptions(context, argc, argv);
	context.in.open(context.transpilerOptions.inputFile);
	context.out.open(context.transpilerOptions.outputFile);
	parse_statement(context, {});
    context.out << "// Prolog\n";
    codeGen_makeProlog(context, context.out);
    context.out << "// Body\n";
    context.out << context.body.str();
    context.out << "// Epilog\n";
    codeGen_makeEpilog(context, context.out);
    return static_cast<int>(transpiler_done(context));
}
