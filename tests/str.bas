print "Test 1 = string compare"
print "Equal?", "a" + "b" = "ab"

print "Test 2 - Simple concatenation"
a$ = "one " + "two " + "three " + "four."
print "Concatenated: ";a$

print "Test 3 - Self referential Incremental cat"
c$ = "one"
c$ = c$ + " two."
print "Result: ";c$

print "Test 4 - Concatenation with chr$ function:"
b$ = "one and " + chr$(64) + " and two and " + "three"
print "Result: ";b$

print "Test 4 multi relop"
print "Result:" ; "a" = chr$(97) = 1 

print "Parenthesised expression: "; ( "a" + "b" + chr$( 64 ) )
