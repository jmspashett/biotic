print "control block test"

x = 10

print "control start" : CONTROL : print "x = "; x
	x = x -1

	for y = 1 to 2
		print "\ty = "; y	
		if x = 5 then LEAVE 
	next

	if x <> 0 then AGAIN
END CONTROL : print "control end"


print "A while loop:"
a = 4
CONTROL : if a = 0 then LEAVE
    print a
    a = a - 1
AGAIN : END CONTROL

print "Do loop:"
x = 1
CONTROL
    print x
    if x = 4 then LEAVE
    x = x + 1
AGAIN : END CONTROL

print "A For loop - no it's not really is it:"
X=1 : CONTROL : IF x = 4 LEAVE
    print X
X = X + 1 : AGAIN : END CONTROL

print "A structured exit block:"
CONTROL
    err = 1
    if error <> 0 then LEAVE
    err = 2
    if error <> 0 then LEAVE
    print "All good"
END CONTROL

Print "A middle exit loop"
x = 0
CONTROL
    x = x + 1
    print x
    if x <> 5 then LEAVE
    print x * x
AGAIN : END CONTROL
