PRINT "Operator tests"

rem Modulo
PRINT "10 mod 4 = "; 10 mod 4

rem Power
PRINT "2 ^ 8 = " 2^8 

rem Mod
PRINT "10 mod 6 = " 10 MOD 6

rem Mod and inequality precedence
print "mod and inequality: " 4 MOD (3 <> 2)
print "mod and inequality: " 4 MOD 3 <> 2
