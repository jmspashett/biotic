
x = 1 : rem to test to see if volatile is applied, due to setjmp

control
    leave
    print "control"
end control

print "gosub tests"
gosub 100
goto stack_overflow

100 print "calling fish"
    gosub fish
    print "return from 100"
    return

fish: 
    print "I am a fish"
    print "return from fish"
    return


stack_overflow:
    print "gosub stack_overflow"
    gosub stack_overflow
