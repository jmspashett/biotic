seed 42

a$ = "fish"

x = 5 : y = 2 : z = 3
print "real array of: ", x, y, z
dim a(x, y, z)

ofs = 0
for k = 0 to z
	for j = 0 to y
		for i = 0 to x
			a(i, j, k) = ofs
			ofs = ofs + 1
		next i
	next j
next k
print "a(0, 0, 1) = "; a(1, 0, 0)
print "a(2, 1, 1) = "; a(2, 1, 1)
print "a(5,2,3) = "; a(5, 2, 3)

print "Automatic array a$ should have a default size of 10, 10, 10"
a$(5,2,3) = "a fish bat"
a$(10, 10, 10) = "a bat fink"
print "a$(5,2,3) = "; a$(5, 2, 3)
print "a$(10,10,10) = "; a$(10, 10, 10)

dim s$(2, 5)

s$(0,0) = "woof, woof."
print s$(0,0)


e$(1, 5) = "15"
print "e$(1, 5)"; e$(1,5)

rem print "Array out of bounds"
rem d$(1) = "5"
rem print "d$(2)"; d$(2)

rem Multiple dimensions in one dim statement
dim z(3), y(3), x(3,3)
