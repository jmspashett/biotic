seed 42

print "string assign test"
a$ = "a fish"
if a$ <> "a fish" then stop

print "string output format tests"
print "a$ is:" ; "(" ;a$ ; ")"
print "lollypop";"stick"
print "suck"
print 1 2
print 1;2

print "for loop test with warning on next"
for r = 3 to 10
    print "r is",r; : print
next r


print "number with exponent pass through"
n = 1E0
print n

print "simple loop and eval"
loop1: 
    print "The cat let the bat out."
    let n = n + 0.5 - 2 + 3
    if n = 0 then stop 
    if n < 5 then goto loop1

print "bracketed expression"
x = ( 4 + 3 ) * (6 + 2)
print x

print "if without statement"
if 5 then

print "X"
print "Z"
print "i"; "j"
print "a"; : print "b"
x=1+1
print x


print "goto test"
1 goto 2
2 print "line" ; 2

10   rem print tests
20   print
30   print "a" "b"
40   print "x" ; "y"
50   print "x" , "y"
60   print "I" ;
70   print "j"
80   k = 4
90   print 4 ; k
100  
110  for y = 2 to 3
120      for x = 1 to 4
130          print x * y
140      next
150      print ""
         r = 2
160  next
170  
print "let test"
280  let hello$ = "Hello, World."
290  print hello$

print "str assign and comparison test"
a$ = "5"
if a$ = "5" then print "five"
if a$ <> "5" then print "NOT five"
if a$ < "9" then print "Less than 10"
if a$ > "3" then print "Greater than 3"

print "chr$ function test"
a$ = "a" + chr$(97) + "b" + "f"
print "a$ is:" ; "(" ;a$ ; ")"
x$ = "a" + a$ 
x$ = "a" + a$ + "c"
a$ = "test " + chr$(65)
print "ZZ1: a$",a$

print "random number tests"
newrand:
    if rnd = rnd goto newrand
print "rnd"; chr$( 65 +  rnd * 26  )
print "rnd"; chr$( 65 + ( rnd * 26 ) )

