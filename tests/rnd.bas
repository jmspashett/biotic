seed 42

PRINT "RND TEST"

limits = 0
CONTROL
    x = RND(0)
rem    PRINT "RND(0)", x
    if x = 0 then limits = limits + 1
    if x = 1 then limits = limits +1
    time_remain = 60 - x * 59
if time_remain > 3 then LEAVE
    if time_remain = 60 then time_remain = 0
rem  print int(time_remain) ; "seconds remaining"
    sleep 0.9
    if limits <> 2 then AGAIN
END CONTROL
PRINT "RND(0) passed or skipped"

PRINT "RND(-1)", RND(-1)
PRINT "RND(1)", RND(1)
PRINT "RND(1)", RND(1)
PRINT
PRINT "RND(-2)", RND(-2)
PRINT "RND(1)", RND(1)
PRINT "RND(1)", RND(1)
