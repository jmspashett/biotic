seed 42

a = 5
print "a = ", a

a = 1 + 2
print "a = ", a

a = 2 * 3
print "a = ", a

a = 2 * 3 + 1 * 5
print "a = ", a

print "a" + "b" = "ab" , "Should be true"

print "a" + "b" = "a" + "b", "Should be true also"

LET C$ = CHR$( 64 + INT( RND * 26 + 1 ) )
print "C$ = " C$

PRINT "0.5 * 4 * 8 = ", 0.5 * 4 * 8


V = -1 : PRINT "V = -1 is: " ; V

print "(-1) = ", (-1)
