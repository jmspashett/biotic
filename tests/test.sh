#!/bin/sh
basic=../biotic
basic_options="+log.lexTokens"
./clean.sh
CFLAGS="-g -O0"
LIBS="-lcurses -lm"
name=$1
if [ -z "$name" ]; then
    name=*.bas
fi
for x in $name; do
    code=$x
    expected="${code}.expected"
    result="${code}.result"
    log="$code.log"
    timeout 5 $basic $basic_options $code "${code}.c" > $log 2>&1
    basic_error=$?
    grep -v tokens: $log > $result
    if [ $basic_error -le 1 ]; then
        if [ -s ${code}.c ]; then
            cc $CFLAGS "${code}.c" -o "${code}.c.out" $LIBS
            input="/dev/stdin"
            if [ -f $code.input ]; then input=$code.input; fi
            timeout --foreground 5 "./${code}.c.out" >> $result 2>&1 < $input
        fi
    fi
    diff -u $expected $result
done
