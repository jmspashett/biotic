
#ifdef WIN32
#include <windows.h>
#elif _POSIX_C_SOURCE >= 199309L
#include <time.h>  
#else
#include <unistd.h>
#endif

void sleep_s(float seconds)
{
#ifdef WIN32
    Sleep(seconds * 1000 );
#elif _POSIX_C_SOURCE >= 199309L
    struct timespec ts;
    const float whole_seconds = (int)seconds;
    ts.tv_sec = whole_seconds;
    ts.tv_nsec = (seconds - whole_seconds) * 1000000000L;
    nanosleep(&ts, NULL);
#else
    usleep(seconds * 1000);
#endif
}
