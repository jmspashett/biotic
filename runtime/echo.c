#include <stdio.h>

#if defined(__unix__) || defined(__unix) || \
    (defined(__APPLE__) && defined(__MACH__))
#define UNIX_IMPL
#endif


#ifdef UNIX_IMPL

#include <termios.h>
#include <signal.h>
#include <sys/select.h>
#include <sys/wait.h>
#include <unistd.h>
#include <assert.h>

void echo(int on)
{
    if (!isatty(STDIN_FILENO)) 
        return;
    struct termios orig_termios;
    if (tcgetattr(STDIN_FILENO, &orig_termios)) {
        perror("tcgetatttr");
        return;
    }

    struct termios change_echo = orig_termios;
    change_echo.c_lflag &= ~ECHO;
    change_echo.c_lflag |= on ? ECHO : 0;
    if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &change_echo)) {
        perror("tcsetatttr");
        return;
    }
}


void save_term(struct termios * term)
{
    if (!isatty(STDIN_FILENO)) 
        return;
    if (tcgetattr(STDIN_FILENO, term)) {
        perror("save_term(): tcgetatttr");
        return;
    }
}


void restore_term(struct termios * term)
{
    if (!isatty(STDIN_FILENO)) 
        return;
    if (tcsetattr(STDIN_FILENO, TCSAFLUSH, term)) {
        perror("tcsetatttr");
        return;
    }
}


void init_term()
{
    pid_t child_pid = fork();
    if (child_pid) {
        /* Restore terminal on child exit */
        static struct termios term;
        save_term(&term);
        sigset_t sigset;
        sigemptyset(&sigset);
        sigaddset(&sigset, SIGCHLD);
        const int err_sig = sigprocmask(SIG_SETMASK, &sigset, NULL);
        assert(err_sig == 0);
        int wstatus;
        fflush(stderr);
        wait(&wstatus);
        restore_term(&term);
        exit(WEXITSTATUS(wstatus));
    }
    echo(0);
}

#else
#warn echo not implemented, see echo.c
void echo(int)
{
}

void init_echo()
{
}
#endif


