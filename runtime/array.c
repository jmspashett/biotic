#include <stdlib.h>
#include <stdio.h>

static int reportMemoryAllocation;

struct array_spec 
{
    size_t n;
    size_t *dims;
    size_t elemSize;
} spec;

struct array_descriptor
{
    const struct array_spec * spec;
    union {
        char str[256];
        float real;
    } * data;
};


void dimension_array(volatile struct array_descriptor * ad)
{
    const size_t dimSize = ad->spec->n;
    size_t allocSize = ad->spec->elemSize;
    if (reportMemoryAllocation)
        fprintf(stderr, "dimension_array: elemSize %zu\n", allocSize);

    for (size_t i = 0; i < dimSize; i++) {
        const size_t x = ad->spec->dims[i];
        allocSize *= x;
    }
    if (reportMemoryAllocation)
        fprintf(stderr, "allocating %zu\n", allocSize);
    ad->data = calloc(allocSize, 1);
}

void * array_access(volatile const struct array_descriptor * ad, 
    const size_t n, ...)
{
    va_list args;
    va_start(args, n);
    va_end(args);

    if (n != ad->spec->n) {
        fprintf(stderr, "array: incorrect number of dimensions\n");
        abort();
    } 
    
    size_t ofs = 0;
    size_t mul = 1;
    for (size_t i = 0; i < n; i++) {
        const size_t d = ad->spec->dims[i];
        const size_t x = va_arg(args, size_t);
        if (x >= d) {
            fprintf(stderr, "array: index out of range %zu %zu\n", x, d);
            fflush(NULL);
            abort();
        }
        ofs += x * mul;
        mul *= d;
    }
    return ad->data + ofs;
}
