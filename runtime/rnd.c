#include <stdlib.h>
#include <time.h>

float rnd(int val) 
{
    if (val < 0)
        srand(abs(val));
    if (val != 0) 
        return ((float)rand() / RAND_MAX);
    else
        return time(NULL) % 60 / 59.0;
}
