
#include <stdio.h>

const char * read_char_into_str()
{
    static char key[2];
    int x = getchar();
    key[0] = x; key[1] = '\0';
    return key;
}

#if defined(__unix__) || defined(__unix) || \
    (defined(__APPLE__) && defined(__MACH__))

#include <termios.h>
#include <sys/select.h>

const char * get_str()
{
    struct termios orig_termios;
    tcgetattr(0, &orig_termios);
    struct termios nonblock = orig_termios;
    cfmakeraw(&nonblock);
    tcsetattr(0, TCSANOW, &nonblock);
    fd_set readfds;
    FD_ZERO(&readfds);
    FD_SET(0, &readfds);
    struct timeval tv = {0};
    const int s = select(1, &readfds, NULL, NULL, &tv);
    const char * key = "";
    if (s) {
        key = read_char_into_str();
    }
    tcsetattr(0, TCSANOW, &orig_termios);
    return key;
}
#else
#warn get not implemented, see get.c
char * get_str()
{
    return read_char_into_str();
}
#endif

#include <stdlib.h>
float get_real()
{
    return atoi(get_str());
}
