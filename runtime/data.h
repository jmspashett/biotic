
struct data_def
{
    enum { Type_Unkown, Type_str, Type_Number, Type_real, Type_Label } type;
    union
    {
        char * str;
        float real;
    } data;
};
