
size_t data_index = 0;

void check_access(int type)
{
    if (data_index >= data_len) {
        fprintf(stderr, "?read: out of data at index %zu, data len %zu\n", 
            data_index, data_len);
        abort();
    }
    if (data_array[data_index].type != type) {
        fprintf(stderr, "read: wrong type at data index %zu\n", data_index);
        abort();
    }
}

char *read_str()
{
    check_access(Type_str);
    return data_array[data_index++].data.str;
}

float read_real()
{
    check_access(Type_real);
    return data_array[data_index++].data.real;
}


void restore()
{
    data_index = 0;
}
