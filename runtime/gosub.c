jmp_buf jmp_bufs[16];
size_t jmp_index = sizeof jmp_bufs / sizeof jmp_bufs[0];

void stack_overflow()
{
    fprintf(stderr, "gosub: out of stack space\n");
    exit(1);
}
