.SUFFIXES:
.SUFFIXES: .bas .c .cpp

.SECONDARY: 

CPPFLAGS += -Wall -std=c++14 
CFLAGS += -lm

.cpp:
	c++ $(CPPFLAGS) -o $@ $<
.c:
	cc $(CFLAGS) -o $@ $<
.bas.c: 
	timeout 5 ./biotic $(BIOTIC_ARGS) $< $@

.PHONY: all
all: tags tests list

.PHONY: list
list:
	@echo Targets are:
	@for x in $$(find . -path ./tests -prune -o -name "*.bas" -printf '%P\n');\
	do\
		echo -e \\t make $$(echo $$x | cut -f1 -d.);\
	done

.PHONY: tests 
tests: biotic
	@echo "Running tests"
	@(cd tests; ./test.sh)

.PHONY: tags
tags: 
	ctags -R .

.PHONY: clean
clean:
	rm -f test.c
	rm -f *.c
	rm -f norvig/*.c
	rm -f *.o
	find . -type f -perm -u=x -regex "./[^.]+$$" -delete


	
